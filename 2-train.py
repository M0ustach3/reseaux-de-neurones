import json
import os

import torch

import libTP
import pandas
import pytorch_lightning

with open('conf/config.json', 'r') as file:
    conf = json.load(file)

if not os.path.exists('conf/model/'):
    os.makedirs('conf/model/')

transforms = libTP.feature_engineering.helpers.load_transforms(conf)

data = pandas.read_csv('dataset/train.csv')

model = libTP.models.AutoEncoder(conf["network"])

data = libTP.feature_engineering.helpers.transform_df(data, transforms)

data = libTP.misc.dataset.PandasDataset(data)
train, validation, test = data.split()

early_stopping = pytorch_lightning.callbacks.EarlyStopping(min_delta=0.01, patience=10, monitor='val_loss')
checkpoints = pytorch_lightning.callbacks.ModelCheckpoint(monitor='val_loss', dirpath='conf/checkpoints/',
                                                          filename='cybersecurity-{epoch:02d}-{val_loss:.2f}', )

trainer = pytorch_lightning.Trainer(max_epochs=1000, callbacks=[early_stopping, checkpoints])

trainer.fit(model, train_dataloader=libTP.misc.dataset.batch_loader(train),
            val_dataloaders=libTP.misc.dataset.batch_loader(validation))

model = model.load_from_checkpoint(checkpoints.best_model_path)

model.calibrate(libTP.misc.dataset.batch_loader(test))

with open('conf/model/saved_model.tr', 'wb') as file:
    torch.save(model, file)

model.save_scorer('conf/model/')
