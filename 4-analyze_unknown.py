import json

import pandas
import torch
import libTP
import numpy
from sklearn import metrics

with open('conf/config.json', 'r') as file:
    conf = json.load(file)

with open('conf/model/saved_model.tr', 'rb') as file:
    model = torch.load(file)

transforms = libTP.feature_engineering.helpers.load_transforms(conf)

data = pandas.read_csv('dataset/unknown.csv')

df = data.drop(columns=["hidden_label"])

transformed_data = libTP.feature_engineering.helpers.transform_df(df, transforms)
transformed_data = libTP.misc.PandasDataset(transformed_data)

model.load_scorer("conf/model/")

result = pandas.DataFrame()

with open("conf/model/threshold.txt", "r") as file:
    threshold = float(file.readline())

result["score"] = model(transformed_data[:]).detach().numpy()
result["pred_label"] = result["score"].copy().map(lambda x: 0 if x < threshold else 1)
result["hidden_label"] = data["hidden_label"].copy()

result.to_csv('result.csv', columns=["score", "pred_label", "hidden_label"], index=False)
