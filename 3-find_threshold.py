import json

import pandas
import torch
import matplotlib.pyplot as plt
import libTP
import numpy
from sklearn import metrics


def perf_measure(y_actual, y_hat):
    """
    Fonction utilisée pour compter le nombre de faux positifs, etc.
    :param y_actual: vraies valeurs
    :param y_hat: valeurs prédites par le modèle
    :return: tp, fp, tn, fn
    """
    tp = 0
    fp = 0
    tn = 0
    fn = 0

    for i in range(len(y_hat)):
        if y_actual[i] == y_hat[i] == 1:
            tp += 1
        if y_hat[i] == 1 and y_actual[i] != y_hat[i]:
            fp += 1
        if y_actual[i] == y_hat[i] == 0:
            tn += 1
        if y_hat[i] == 0 and y_actual[i] != y_hat[i]:
            fn += 1

    return tp, fp, tn, fn


with open('conf/config.json', 'r') as file:
    conf = json.load(file)

with open('conf/model/saved_model.tr', 'rb') as file:
    model = torch.load(file)

transforms = libTP.feature_engineering.helpers.load_transforms(conf)

data = pandas.read_csv('dataset/evaluate.csv')

transformed_data = libTP.feature_engineering.helpers.transform_df(data, transforms)
transformed_data = libTP.misc.PandasDataset(transformed_data)

model.load_scorer("conf/model/")
data["score"] = model(transformed_data[:]).detach().numpy()

max_value = float(numpy.max(data["score"]).round())
min_value = float(numpy.min(data["score"]).round())

threshold_calc = []
f1_calc = []

data["attack_cat"] = data["attack_cat"].map(lambda x: 0 if x == "Normal" else 1)

for threshold in numpy.arange(min_value, max_value, 0.1):
    data["prediction"] = data["score"].copy()
    data["prediction"] = data["prediction"].map(lambda x: 0 if x < threshold else 1)
    f1 = metrics.f1_score(data["attack_cat"], data["prediction"])
    threshold_calc.append(threshold)
    f1_calc.append(f1)

fig, ax = plt.subplots()
ax.set_ylabel('F1 Score')
ax.set_xlabel('Threshold')
ax.set_title('F1 Score en fonction du threshold')
ymax = max(f1_calc)
xmax = threshold_calc[f1_calc.index(ymax)]
ax.annotate("(" + str(xmax) + ", " + str(round(ymax, 2)) + ")", xy=(xmax, ymax), xytext=(xmax - 2, ymax))

ax.plot(threshold_calc, f1_calc)
plt.show()
fig.savefig('report/img/f1_score.png')

predicted = data["score"].copy()
predicted = predicted.map(lambda x: 0 if x < xmax else 1)

(TP, FP, TN, FN) = perf_measure(data["attack_cat"].tolist(), predicted.tolist())

print("True positive : ", TP)
print("False positive : ", FP)
print("True negative : ", TN)
print("False negative : ", FN)

with open("conf/model/threshold.txt", "w") as file:
    file.write(str(xmax))
