import json
import os
import pandas as pd

from libTP.feature_engineering import CategoricalFE, BinaryFE, NumericalFE
from libTP.feature_engineering.helpers import save_transforms

conf = {
    "attributes": {},
    "network": {}
}

dataframe = pd.read_csv("dataset/train.csv")

for attr in ['dur', 'spkts', 'dpkts', 'sbytes',
             'dbytes', 'rate', 'sttl', 'dttl', 'sload', 'dload', 'sloss', 'dloss',
             'sinpkt', 'dinpkt', 'synack', 'ackdat', 'smean', 'dmean', 'trans_depth',
             'response_body_len', 'ct_srv_src', 'ct_dst_ltm', 'ct_src_dport_ltm',
             'ct_dst_sport_ltm', 'ct_dst_src_ltm', 'ct_ftp_cmd',
             'ct_flw_http_mthd', 'ct_src_ltm', 'ct_srv_dst']:
    conf["attributes"][attr] = "numerical"

for attr in ['proto', 'state', 'service']:
    conf["attributes"][attr] = "categorical"

for attr in ['is_sm_ips_ports', 'is_ftp_login']:
    conf["attributes"][attr] = "binary"

transforms = {}

for attribute in conf["attributes"]:
    if conf["attributes"][attribute] == 'numerical':
        transforms[attribute] = NumericalFE()
    elif conf["attributes"][attribute] == 'binary':
        transforms[attribute] = BinaryFE()
    else:
        transforms[attribute] = CategoricalFE()
    transforms[attribute].fit(dataframe[attribute])


for attribute in conf["attributes"]:
    conf["network"][attribute] = {}
    if conf["attributes"][attribute] == 'numerical':
        conf["network"][attribute]["type"] = 'numerical'
        conf["network"][attribute]["output_size"] = 3
    elif conf["attributes"][attribute] == 'binary':
        conf["network"][attribute]["type"] = 'binary'
        conf["network"][attribute]["output_size"] = 2
    else:
        conf["network"][attribute]["type"] = 'categorical'
        conf["network"][attribute]["output_size"] = transforms[attribute].max_size + 1
        conf["network"][attribute]["max_size"] = transforms[attribute].max_size

if not os.path.exists('conf/'):
    os.makedirs('conf/')

with open("conf/config.json", 'w') as file:
    json.dump(conf, file)

save_transforms(transforms)
