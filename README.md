# TP de Réseaux de Neurones

## ESIEA 4A

### Pablo BONDIA-LUTTIAU, Louis JOUCLAS et Roxane TISSIER

### Installation

Nous avons utilisé Python 3.8 pour le développement du projet. Ainsi, pour créer directement
un environnement conda avec les paquets présents, faire :

```bash
conda create --name myenv_for_tp
```

Puis lancer le script ou copier/coller les instructions une par une dans le fichier conda_setup.sh :

```bash
bash conda_setup.sh
```

Cela installera les dépendances nécessaires, dans les bonnes versions.