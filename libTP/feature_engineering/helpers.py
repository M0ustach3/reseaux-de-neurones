import os

from .categorical import CategoricalFE
from .numerical import NumericalFE
from .binary import BinaryFE
import pandas as pd


def save_transforms(transforms: dict, base_path: str = "./conf/fe") -> None:
    """
    Méthode permettant de sauvegarder des Feature Encoders sur le disque
    :param transforms: dictionnaire sous la forme {attribut: FE}
    :param base_path: le dossier dans lequel sauvegarder les FE. Par défaut ./conf/fe
    :return:
    """
    if not os.path.exists(base_path):
        os.makedirs(base_path)
    for key in transforms:
        transforms[key].save(base_path + '/' + key + '-fe.pickle')


def load_transforms(conf: dict, base_path: str = './conf/fe') -> dict:
    """
    Méthode permettant de charger des Feature Encoders depuis un dossier sur le disque
    :param conf: Fichier de configuration décrivant les différents Feature Encoders
    :param base_path: Le dossier dans lequel lire les fichiers
    :return: un dictionnaire sous la forme {attribut: FE}
    """
    transforms = {}

    for key in conf["attributes"]:
        if conf["attributes"][key] == "categorical":
            transforms[key] = CategoricalFE()
        elif conf["attributes"][key] == "numerical":
            transforms[key] = NumericalFE()
        else:
            transforms[key] = BinaryFE()
        transforms[key].load(base_path + '/' + key + '-fe.pickle')

    return transforms


def transform_df(df: pd.DataFrame, transforms: dict) -> pd.DataFrame:
    """
    Méthode permettant de transformer un DataFrame complet en utilisant le FE correct pour chaque type de donnée
    :param df: le DataFrame à transformer
    :param transforms: un dictionnaire sous la forme {attribut : FE} faisant la correspondance entre
    l'attribut du DataFrame et quel FE utiliser pour celui-ci
    :return: une DataFrame modifiée
    """
    res = pd.DataFrame()
    for attr, fe in transforms.items():
        res[attr], data_to_delete = fe(df[attr])
        if data_to_delete:
            res[attr].drop(data_to_delete)
    return res
