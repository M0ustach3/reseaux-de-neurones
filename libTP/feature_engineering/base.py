import pickle
import pandas as pd


class BaseFE:
    """
    Classe représentant un Feature Encoder de base
    """

    def __init__(self, **kwargs):
        """
        Constructeur
        :param kwargs: Arguments
        """
        pass

    def save(self, path: str) -> None:
        """
        Méthode utilisée pour sauvegarder le Feature Encoder dans un fichier avec pickle
        :param path: Le fichier à écrire
        :return:
        """
        with open(path, 'wb') as f:
            pickle.dump(vars(self), f)

    def load(self, path: str) -> None:
        """
        Méthode utilisée pour charger le Feature Encoder depuis un fichier pickle
        :param path: Le fichier à charger
        :return:
        """
        with open(path, 'rb') as f:
            data = pickle.load(f)
            for k, v in data.items():
                setattr(self, k, v)

    def fit(self, data: pd.Series) -> None:
        """
        Méthode utilisée pour adapter le Feature Encoder aux données fournies par le paramètre
        :param data: Les données sur lesquelles s'adapter
        :return:
        """
        raise NotImplementedError()

    def transform(self, data: pd.Series) -> pd.Series:
        """
        Méthode utilisée pour transformer les données avec les paramètres du Feature Encoder
        :param data: Les données à transformer
        :return: Les données transformées par le FE
        """
        raise NotImplementedError()

    def fit_transform(self, data: pd.Series) -> pd.Series:
        """
        Méthode combinant la méthode fit et la méthode transform
        :param data: les données sur lesquelles s'adapter et transformer
        :return: les données transformées
        """
        self.fit(data)
        return self.transform(data)

    def __call__(self, data: pd.Series) -> pd.Series:
        """
        Méthode appellée par défaut si on fait appel à la classe directement.
        Cette méthode transforme les données fournies en paramètres
        :param data: les données à transformer
        :return: les données transformées
        """
        return self.transform(data)
