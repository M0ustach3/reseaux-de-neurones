from typing import Tuple

from .base import BaseFE
import pandas as pd
import numpy as np


class CategoricalFE(BaseFE):
    """
    Feature Encoder pour les données catégorielles
    """

    def __init__(self, max_size: int = None):
        """
        Constructeur
        :param max_size: La taille maximale des catégories
        """
        super().__init__()
        self.association_dict = {}
        self.max_size = max_size

    def fit(self, data: pd.Series) -> None:
        """
        Méthode permettant de créer la correspondance entre une valeur numérique et une catégorie
        :param data: les données sur lesquelles s'adapter
        :return:
        """
        if not isinstance(self.max_size, int):
            self.max_size = len(data.unique()) + 1
        # value_counts will sort unique values from the most frequent to the least
        for v in data.value_counts().index:
            # We don't want to store more associations than max_size allows
            if len(self.association_dict) + 1 >= self.max_size:
                break
            # Associate an integer to each possible category
            if v not in self.association_dict:
                self.association_dict[v] = len(self.association_dict)

    def transform_fn(self, x: str) -> int:
        """
        Méthode de transformation à appliquer sur tous les éléments des données
        :param x: L'élément à traiter
        :return: Retourne le numéro de la catégorie correspondant au nom de la catégorie, ou renvoie max_size - 1
        si l'élément ne fait pas partie du dictionnaire
        """
        return self.association_dict[x] if x in self.association_dict else self.max_size - 1

    def transform(self, data: pd.Series) -> Tuple[pd.Series, list]:
        """
        Méthode transformant toutes les données passées en paramètres
        :param data: les données à transformer
        :return: les données transformées
        """
        return data.map(self.transform_fn).astype(np.long), []
