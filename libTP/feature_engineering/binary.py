from typing import Tuple

from .base import BaseFE
import numpy as np
import pandas as pd


class BinaryFE(BaseFE):
    """
    Feature Encoder pour les données binaires
    """

    def __init__(self):
        """
        Constructeur
        """
        super().__init__()
        pass

    def fit(self, data: pd.Series) -> None:
        """
        Méthode qui override la méthode parente, ne fait rien car un Feature Encoder de données
        binaires n'a pas de paramètres
        :param data: les données sur lesquelles s'adapter
        :return:
        """
        pass

    def transform(self, data: pd.Series) -> Tuple[pd.Series, list]:
        """
        Méthode de transformation des données binaires.
        :param data: les données à transformer
        :return: les données transformées
        """
        # On cherche les données qui ne sont pas dans 0 et 1
        indexes = np.isin(data.to_numpy(), [0, 1])
        need_to_delete = []
        # Le tableau contient [True, False, ..., True, True]
        for index, value in enumerate(indexes):
            # Si la valeur est False
            if not value:
                # L'index de la ligne doit être supprimé car la valeur n'est ni 0 ni 1
                need_to_delete.append(index)
        return data.astype(np.float32), need_to_delete
