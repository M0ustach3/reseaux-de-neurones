from typing import Tuple

import numpy
import numpy as np
from .base import BaseFE
import pandas as pd


class NumericalFE(BaseFE):
    """
    Feature Encoder pour les données numériques
    """

    def __init__(self):
        """
        Constructeur.
        Initialise les champs mean et sigma à 0
        """
        super().__init__()
        self.centile = 0
        self.maximum = -100

    def fit(self, data: pd.Series) -> None:
        """
        Méthode utilisée pour trouver les paramètres pour des données numériques
        :param data: les données sur lesquelles trouver les paramètres
        :return:
        """
        self.centile = numpy.percentile(data.to_numpy(), 90)
        self.maximum = numpy.amax(data)

    def transform_fn(self, x):
        """
        Méthode de transformation pour map
        :param x: l'élément à modifier
        :return: l'élément standardisé
        """
        return (x - self.centile) / self.maximum

    def transform(self, data: pd.Series) -> Tuple[pd.Series, list]:
        """
        Méthode permettant de transformer les données fournies en paramètres
        Pandas arrive à traiter automatiquement les soustractions et divisons entre une série et un entier
        Il parcourt simplement le tableau en appliquant la soustraction et division sur chaque élément
        :param data: les données à transformer
        :return: les données transformées
        """
        return data.map(self.transform_fn).astype(np.float32), []
