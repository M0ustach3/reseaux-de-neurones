import torch


class BinaryEncoder(torch.nn.Module):
    """
    Classe représentant un encodeur binaire
    """

    def __init__(self, params):
        """
        Constructeur
        :param params: les paramètres de cet encodeur particulier
        """
        super().__init__()
        self.sequential = torch.nn.Sequential(
            torch.nn.Linear(1, params["output_size"]),
            torch.nn.ReLU()
        )

    def forward(self, x):
        """
        Méthode permettant de faire une passe en avant dans l'encodeur
        :param x: le tenseur d'entrée
        :return: le tenseur sorti par le sequential
        """
        return self.sequential(x)


class BinaryDecoder(torch.nn.Module):
    """
    Classe représentant un décodeur binaire
    """

    def __init__(self, input_dim, params):
        """
        Constructeur.
        :param input_dim: la dimension d'entrée du décodeur, qui est la dimension de sortie de la couche bottleneck
        :param params: non utilisé
        """
        super().__init__()
        self.sequential = torch.nn.Sequential(
            torch.nn.Linear(input_dim, 1),
            torch.nn.Sigmoid(),
        )

    def forward(self, x):
        """
        Méthode permettant de faire une passe en avant dans l'encodeur
        :param x: le tenseur d'entrée
        :return: le tenseur sorti par le sequential
        """
        return self.sequential(x)


class BinaryLoss(torch.nn.Module):
    """
    Classe représentant le loss binaire
    """

    def __init__(self, params):
        """
        Constructeur
        :param params: non utilisé
        """
        super().__init__()
        self.loss = torch.nn.BCELoss(reduction="none")

    def forward(self, yh, y):
        """
        Méthode permettant de calculer le loss binaire
        :param yh: la valeur prédite par le modèle
        :param y: la valeur attendue
        :return: la perte entre les deux valeurs
        """
        return self.loss(yh, y)
