import torch


class CategoricalEncoder(torch.nn.Module):
    """
    Classe représentant un encodeur catégorique
    """

    def __init__(self, params):
        """
        Constructeur
        :param params: les paramètres du réseau, max_size est la taille du vocabulaire d'entrée
        """
        super().__init__()
        self.sequential = torch.nn.Sequential(
            torch.nn.Embedding(params["max_size"], params['output_size']),
            torch.nn.Sigmoid()
        )

    def forward(self, x):
        """
        Méthode permettant de faire une passe en avant dans l'encodeur
        :param x: le tenseur d'entrée
        :return: le tenseur sorti par le embedding et le sigmoid
        """
        return self.sequential(x).squeeze()


class CategoricalDecoder(torch.nn.Module):
    """
    Classe représentant un décodeur catégorique
    """

    def __init__(self, input_dim, params):
        """
        Constructeur
        :param input_dim: la dimension d'entrée du décodeur, qui est la dimension de sortie de la couche de
        bottleneck
        :param params: les paramètres du réseau
        """
        super().__init__()
        self.sequential = torch.nn.Sequential(
            torch.nn.Linear(input_dim, params["max_size"]),
            torch.nn.Softmax()
        )

    def forward(self, x):
        """
        Méthode permettant de faire une passe en avant dans le décodeur
        :param x: le tenseur d'entrée
        :return: le tenseur sorti par le sequential
        """
        return self.sequential(x)


class CategoricalLoss(torch.nn.Module):
    """
    Classe représentant le loss catégorique
    """

    def __init__(self, params):
        """
        Constructeur
        :param params: non utilisé
        """
        super().__init__()
        self.crossentropy = torch.nn.CrossEntropyLoss(reduction="none")

    def forward(self, yh, y):
        """
        Méthode permettant de calculer le loss catégorique
        :param yh: la valeur prédite par le modèle
        :param y: la valeur attendue
        :return: la perte entre les deux valeurs
        """
        return self.crossentropy(yh, y.squeeze())
