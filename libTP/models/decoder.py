import torch
from .categorical import CategoricalDecoder
from .numerical import NumericalDecoder
from .binary import BinaryDecoder


class Decoder(torch.nn.Module):
    """
    Classe représentant le décodeur
    """

    def __init__(self, input_variables_dict, input_dim=32):
        """
        Constructeur
        :param input_variables_dict: le dictionnaire de variables représentant la structure du réseau
        :param input_dim: la dimension d'entrée, qui est la dimension de sortie de la couche de bottleneck
        """
        super().__init__()
        self.attribute_decoders = torch.nn.ModuleDict()

        for (attr, params) in input_variables_dict.items():
            if params["type"] == "categorical":
                self.attribute_decoders[attr] = CategoricalDecoder(input_dim=input_dim, params=params)
            elif params["type"] == "binary":
                self.attribute_decoders[attr] = BinaryDecoder(input_dim=input_dim, params=params)
            elif params["type"] == "numerical":
                self.attribute_decoders[attr] = NumericalDecoder(input_dim=input_dim, params=params)
            else:
                raise NotImplementedError("Unsupported attribute type %s" % params["type"])

    def forward(self, x):
        """
        Méthode permettant de faire une passe avant dans le décodeur
        :param x: le tenseur d'entrée
        :return: le tenseur reproduit par le réseau
        """
        decoded = dict()

        # Décode ce qui est produit par le bottleneck
        for attr in self.attribute_decoders:
            decoded[attr] = self.attribute_decoders[attr](x)
        # Renvoie tout
        return decoded
