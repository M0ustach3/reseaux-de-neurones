import torch


class NumericalEncoder(torch.nn.Module):
    """
    Classe représentant un encodeur numérique
    """

    def __init__(self, params):
        """
        Constructeur
        :param params: les paramètres du réseau
        """
        super().__init__()
        self.sequential = torch.nn.Sequential(
            torch.nn.Linear(1, params["output_size"]),
            torch.nn.Tanh()
        )

    def forward(self, x):
        """
        Méthode permettant de faire une passe en avant dans l'encodeur
        :param x: le tenseur d'entrée
        :return: le tenseur sorti par le sequential
        """
        return self.sequential(x)


class NumericalDecoder(torch.nn.Module):
    """
    Classe représentant un décodeur numérique
    """

    def __init__(self, input_dim, params):
        """
        Constructeur
        :param input_dim: la dimension d'entrée du décodeur, qui est la dimension de sortie de la couche de
        bottleneck
        :param params: non utilisé
        """
        super().__init__()
        self.sequential = torch.nn.Sequential(
            torch.nn.Linear(input_dim, 1),
            torch.nn.Tanh(),
        )

    def forward(self, x):
        """
        Méthode permettant de faire une passe en avant dans le décodeur
        :param x: le tenseur d'entrée
        :return: le tenseur sorti par le sequential
        """
        return self.sequential(x)


class NumericalLoss(torch.nn.Module):
    """
    Classe représentant le loss numérique
    """

    def __init__(self, params):
        """
        Constructeur
        :param params: non utilisé
        """
        super().__init__()
        self.loss = torch.nn.MSELoss(reduction="none")

    def forward(self, yh, y):
        """
        Méthode permettant de calculer le loss numérique
        :param yh: la valeur prédite par le modèle
        :param y: la valeur attendue
        :return: la perte entre les deux valeurs
        """
        return self.loss(yh, y)
