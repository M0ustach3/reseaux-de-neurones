from pytorch_lightning.core.lightning import LightningModule
from torch import Tensor
from torch.utils.data import DataLoader

from .encoder import Encoder
from .decoder import Decoder
from torch.nn import Dropout
from .loss import Loss
import torch
import os
import pickle


class AutoEncoder(LightningModule):
    """
    Classe représentant l'autoencodeur
    """

    def __init__(self, input_variables_dict: dict, latent_dim: int = 32):
        """
        Constructeur
        :param input_variables_dict: le dictionnaire de variables ainsi que leur type,
         provenant de la configuration du réseau
        :param latent_dim: la dimension de la couche latente (bottleneck)
        """
        super().__init__()
        self.save_hyperparameters()

        self.encoder = Encoder(input_variables_dict, output_dim=self.hparams.latent_dim)

        self.decoder = Decoder(input_variables_dict, input_dim=self.hparams.latent_dim)

        self.dropout = Dropout()

        self.loss = Loss(input_variables_dict)

        self.scorer_parameters = dict()

    def save_scorer(self, path: str) -> None:
        """
        Méthode utilisée pour sauvegarder les paramètres du scorer de l'autoencodeur
        :param path: le DOSSIER où enregistrer le fichier, qui portera le nom "scorer"
        :return:
        """
        if not os.path.exists(path):
            os.mkdir(path)
        with open("%s/scorer" % path, 'wb') as f:
            pickle.dump(self.scorer_parameters, f)

    def load_scorer(self, path: str) -> None:
        """
        Méthode permettant de charger les paramètres du scorer dans l'autoencodeur
        :param path: le chemin du DOSSIER ou sera chargé le fichier "scorer"
        :return:
        """
        with open("%s/scorer" % path, 'rb') as f:
            self.scorer_parameters = pickle.load(f)

    def forward(self, x):
        """
        Méthode utilisée lors d'une passe avant dans le réseau
        :param x: le tenseur d'input
        :return: le score de la perte entre la valeur initiale et la valeur reconstituée par le réseau
        """
        reprod = self.autoencoder(x)
        loss = self.loss(reprod, x)
        return self.score(loss)

    def autoencoder(self, x):
        """
        Méthode utilisée pour renvoyer la valeur décodée par le réseau
        :param x: le tenseur d'entrée
        :return: le tenseur décodé par le réseau
        """
        encoded = self.encoder(x)
        # If you add layers, don't forget to call them here
        encoded = self.dropout(encoded)
        decoded = self.decoder(encoded)
        return decoded

    def score(self, loss_dict: dict) -> Tensor:
        """
        Méthode utilisée pour calculer le score du réseau, et de détecter les anomalies
        :param loss_dict: le dictionnaire des erreurs
        :return: un score d'anomalie pour un échantillon donné
        """
        scaled_loss = {}
        for attr in loss_dict:
            scaled_loss[attr] = (loss_dict[attr] - self.scorer_parameters[attr]["mu"]) / \
                                self.scorer_parameters[attr]["sigma"]
            scaled_loss[attr] = torch.clip(scaled_loss[attr], 0, 10)
        return torch.sum(torch.stack([v.squeeze() for k, v in scaled_loss.items()]), dim=0)

    def calibration_step(self, batch):
        """
        Méthode représentant une étape de la calibration du modèle
        :param batch: le batch à utiliser
        :return: une perte de reproduction entre le batch d'entrée et la reproduction par le réseau
        """
        reprod = self.autoencoder(batch)
        return self.loss(reprod, batch)

    def calibrate(self, data_loader: DataLoader) -> None:
        """
        Méthode utilisée pour calibrer l'autoencodeur à partir d'un loader
        :param data_loader: le dataloader à utiliser
        :return:
        """
        self.eval()
        with torch.no_grad():
            # Compute loss for each batch
            batches_loss = [self.calibration_step(batch) for batch in data_loader]
            # Concatenate all the losses
            losses = {attr: torch.cat([batch_loss[attr] for batch_loss in batches_loss]) for attr in
                      self.hparams.input_variables_dict}
            for attr in losses:
                if losses[attr].dim() == 1:
                    losses[attr] = losses[attr].unsqueeze(-1)
                self.scorer_parameters[attr] = {
                    "mu": losses[attr].mean(),
                    "sigma": losses[attr].std(),
                }

    def training_step(self, batch, batch_idx):
        """
        Méthode représentant une étape de l'entraînement
        :param batch: le batch à utiliser pour l'entraînement
        :param batch_idx: non utilisé
        :return:
        """
        reprod = self.autoencoder(batch)
        loss_dict = self.loss(reprod, batch)

        # Sum all the losses
        loss = torch.stack([v.squeeze() for _, v in loss_dict.items()]).sum(dim=0)
        # Note: if you add l1/l2 regularisation you will have to add it in the loss

        # Log loss for each attribute
        for attr in loss_dict:
            self.log("%s_loss" % attr, loss_dict[attr].mean())

        return loss.mean()

    def validation_step(self, batch, batch_idx):
        """
        Méthode représentant une étape de validation
        :param batch: le batch à utiliser pour la validation
        :param batch_idx: non utilisé
        :return:
        """
        reprod = self.autoencoder(batch)
        loss_dict = self.loss(reprod, batch)
        # Sum all the losses
        val_loss = torch.stack([v.squeeze() for _, v in loss_dict.items()]).sum(dim=0).mean()

        # Log loss for each attribute
        for attr in loss_dict:
            self.log("%s_val_loss" % attr, loss_dict[attr].mean())

        self.log('val_loss', val_loss, on_epoch=True, prog_bar=True)

    def configure_optimizers(self) -> torch.optim.Adam:
        """
        Méthode permettant de configurer l'optimiseur à utiliser, par défaut Adam,
        mais peut être descente de gradient etc. Learning rate de base de 0.001
        :return:
        """
        return torch.optim.Adam(self.parameters(), lr=0.001)
