from __future__ import annotations

from typing import Tuple
import torch
import pandas as pd
from torch.utils.data import DataLoader, BatchSampler


def dict_collate(batch: list) -> dict:
    """
    Fonction utilisée par le DataLoader
    :param batch: le batch de données
    :return: un dictionnaire
    """
    res = {}
    for row in batch:
        for k in row:
            res[k] = torch.cat([res.get(k, torch.tensor([], dtype=row[k].dtype)), row[k]])

    return res


def batch_loader(dataset: PandasDataset, batch_size: int = 64, num_workers: int = 4) -> DataLoader:
    """
    Fonction utilisée pour créer et renvoyer un dataloader à partir de données
    :param dataset: le dataset à charger
    :param batch_size: la taille du batch
    :param num_workers: le nombre de workers pour le dataloader
    :return: un dataloader avec les données fournies
    """
    sampler = BatchSampler(torch.utils.data.RandomSampler(dataset), batch_size=batch_size,
                           drop_last=False)
    return DataLoader(dataset, sampler=sampler, num_workers=num_workers, collate_fn=dict_collate)


class PandasDataset(torch.utils.data.Dataset):
    """
    Classe utilisée pour simplifier la gestion des datasets
    """

    def __init__(self, df: pd.DataFrame, **kwargs):
        """
        Constructeur
        :param df: la dataframe que l'on veut utiliser
        :param kwargs:
        """
        self.data = df

    def split(self, train=0.6, val=0.2, test=0.2, shuffle=True) -> Tuple[PandasDataset, PandasDataset, PandasDataset]:
        """
        Méthode utilisée pour séparer un PandasDataset en plusieurs parties
        :param train: la proportion du jeu d'entraînement (entre 0 et 1)
        :param val: la proportion du jeu de validation (entre 0 et 1)
        :param test: la proportion du jeu de test (entre 0 et 1)
        :param shuffle: si les données doivent être mélangées
        :return:
        """
        df = self.data.sample(frac=1).reset_index(drop=True)

        train_stop = int(len(self.data) * train)
        val_stop = train_stop + int(len(self.data) * val)
        test_stop = val_stop + int(len(self.data) * test)

        return (PandasDataset(df[:train_stop]),
                PandasDataset(df[train_stop:val_stop]),
                PandasDataset(df[val_stop:test_stop]))

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        # If there is only one index, transform it into a list
        if isinstance(idx, int):
            idx = [idx]

        subset = self.data.iloc[idx]
        return {k: torch.tensor(subset[k].values).unsqueeze(-1) for k in subset.columns}
