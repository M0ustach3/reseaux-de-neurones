#!/bin/bash

conda install pandas scikit-learn;

conda install pytorch torchvision torchaudio cpuonly -c pytorch;

pip install pytorch-lightning\["cpu-extra"\];
